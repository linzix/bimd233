
function calcCircleGeometries(radius) {
    const pi = Math.PI;   
    var area =pi * radius * radius;
    var circumference = 2 * pi * radius;
    var diameter =2 * radius;
    var geometries = [area, circumference, diameter];
    return geometries;
  }
  document.getElementById("one").innerHTML = "Radius 1: " + calcCircleGeometries(1);
  document.getElementById("two").innerHTML = "Radius 2: " + calcCircleGeometries(2);
  document.getElementById("three").innerHTML = "Radius 3: " + calcCircleGeometries(3);