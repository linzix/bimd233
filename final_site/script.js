var url =
  "https://api.nasa.gov/planetary/apod?api_key=xpYgr7RfN4NmAWYMLcbfVbWbXkAWCTp4q1oR1eZc";

$("a.btn").click(function() {
  $.ajax({
    url: url,
    success: function(result) {
      if ("copyright" in result) {
        $("#copyright").text(result.copyright);
      } else {
        $("#copyright").text("Image Credits: " + "Public Domain");
      }

      if (result.media_type == "video") {
        $("#apod_img_id").css("display", "none");
        $("#apod_vid_id").attr("src", result.url);
      } else {
        $("#apod_vid_id").css("display", "none");
        $("#apod_img_id").attr("src", result.url);
      }
    $("#apod_info").text(result.explanation);
      $("#apod_title").text(result.title);
    }
  });
});
var numbers = [10, 20, 30];

document.getElementById("sum").innerHTML = numbers.reduce(getSum);

function getSum(total, num) {
  return total + num;
}